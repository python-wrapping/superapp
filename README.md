# Superapp

This is a very simple test to create a Python app on Haiku using the Qt
framework, and making an executable with Cython that shows its icon on the
Deskbar when running.

PyQt should be installed through HaikuDepot.

I used a VENV with Python3 to test the app, the VENV is not part of the GitLab
project as does not make sense to add it here. The 'requirements.txt' file holds
the required modules needed to add to the environment in order to carry on the
test.

**Details**
1. Create a Python3 virtual environment ('python3 -m venv --system-site-packages
   testenv')
    Need to use PyQt5 module from HaikuDepot, therefore the need for
system-site-packages.
2. Inside the 'testenv' add the Cython module ('python -m pip install Cython')
3. Create the simple GUI qith QtCreator and save it as 'mainwindow.gui'
4. Convert the '.ui' file to '.py' with 'pyuic5 -x mainwindow.ui -o
   mainwindow.py'
5. Test that this Python file works:
    - inside the 'testenv': 'python mainwindow.py'
    - outside the 'testenv': 'python3 mainwindow.py'
6. Create the Icon in Icon-O-Matic, save it as HVIF and export it as RDEF
   ('testicon.rdef')
7. Create a file 'superapp.rdef'. Best option to do this is use Paladin:
    - Launch Paladin and create an empty application project, with nothing
    - Tell Paladin to add a file, and name the file 'superapp.rdef' (this 'RDEF'
      file will have all the needed elements and some icon code we will replace)
    - Replace the 'superapp.rdef' icon code with the one we created in
      'testicon.rdef'
    - Set the application type in the 'superapp.rdef' file to 'x-vnd/Superapp'
8. Convert the 'superapp.rdef' file to '.rsrc' with 'rc superapp.rdef'
9. Use Cython inside the virtual environment ('testenv') to create a '.C' file
   with 'python -m cython --embed superapp.py'
10. Compile the resulting '.C' file with the 'GCC' compiler, outside the virtual
    environment:
    - 'gcc -o Superapp $(python3-config --cflags) $(python3-config --ldflags) ./superapp.c'
11. Set the application icon to the one we created in Step 7 with 'resattr -o
    Superapp superapp.rsrc'

With this the application is executable, in the terminal as well as with double
click showing its icon on the Deskbar, and only needs packaging to integrate it in the Apps menu.

**Final note**
This is a very simple project to test out the process. The code of this project
does not do anything as it's only for testing purposes, so don't expect any
interaction with menus or anything.

The '.ui' file is converted to '.py' also for testing purposes. Actually, can be
loaded within the Python application, though I haven't tested this for this
example. (I'm doing it with a simple app, and will update results).
